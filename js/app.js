/* objetos en javascript */
const automovil = {
    marca: "Chevrolet ",
    modelo: "Corvette",
    descripcion: "2 puertas, DE 0 A 100 EN 3.3 SEG 495HP, Central Visible 6.2L, V8, Inyección Directa",
    imagen: "/img/corvette1.png"
};

// Mostrar Objeto
console.log("automovil ", automovil);
console.log("marca ", automovil.marca);
console.log("modelo ", automovil.modelo);
console.log("descripcion ", automovil.descripcion);
console.log("imagen ", automovil.imagen);

/* Arreglo de objetos */
const automoviles = [
    {
        marca: "TOYOTA",
        modelo: " GR Yaris",
        descripcion: " Tracción en las 4 ruedas, GR-FOUR Motor turbo de 1.6 litros y 3 cilindros 257 HP de potencia",
        imagen: "/img/tyaris.png"
    },
    {
        marca: "Nissan",
        modelo: "Frontier PRO 4X",
        descripcion: "Motor V6. 3.8 Lts con 310 hp y transmisión automática de 9 velocidades, Tracción 4x4 con Shift on the Fly",
        imagen: "/img/Frontier.png"
    },
    {
        marca: "Ford",
        modelo: "Lobo F-150",
        descripcion: "Transmisión Automática de 10 Velocidades, Doble Cabina 4 x 4Motor 5.0L V8",
        imagen: "/img/loboford.png"
    }
];

/* Mostrar todos los arreglos */
for (let i = 0; i < automoviles.length; ++i) {
    console.log("marca: " + automoviles[i].marca);
    console.log("modelo: " + automoviles[i].modelo);
    console.log("descripcion: " + automoviles[i].descripcion);
    console.log("imagen: " + automoviles[i].imagen);
}

const selMarc = document.getElementById('selMarc');

selMarc.addEventListener('change', function() {

    let opcion = parseInt(selMarc.value);
    alert(opcion);

    const modeloElement = document.getElementById("modelo");
    modeloElement.innerHTML = automoviles[opcion].modelo;

    const marcaElement = document.getElementById("marca");
    marcaElement.innerHTML = automoviles[opcion].marca;

    const descripcionElement = document.getElementById("descripcion");
    descripcionElement.innerHTML = automoviles[opcion].descripcion;

    const imagenElement = document.getElementById("imagen");
    imagenElement.src = automoviles[opcion].imagen;
});
